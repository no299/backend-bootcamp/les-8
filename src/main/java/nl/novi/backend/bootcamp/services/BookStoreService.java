package nl.novi.backend.bootcamp.services;

import nl.novi.backend.bootcamp.dto.BookStoreDto;
import nl.novi.backend.bootcamp.exceptions.NotFoundException;
import nl.novi.backend.bootcamp.mappers.BookStoreMappers;
import nl.novi.backend.bootcamp.repositories.BookStoreRepository;
import org.springframework.stereotype.Service;

@Service
public class BookStoreService {

    private final BookStoreRepository bookStoreRepository;

    public BookStoreService(BookStoreRepository bookStoreRepository) {
        this.bookStoreRepository = bookStoreRepository;
    }

    public BookStoreDto getOne(Long id) {
        return bookStoreRepository.findById(id)
                .map(BookStoreMappers::toDto)
                .orElseThrow(NotFoundException::new);
    }
}
