package nl.novi.backend.bootcamp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "We can't find what you're looking for")
public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super("Item not found");
    }
}
