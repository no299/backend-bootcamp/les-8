package nl.novi.backend.bootcamp.repositories;

import nl.novi.backend.bootcamp.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorsRepository extends JpaRepository<Author, Long> {

    //    Optional<Author> findByName(String name);

    @Query("select a from Author a where a.name = :name")
    Optional<Author> findByName(String name);

    @Query("select a from Author a where lower(a.name) = lower(:name)")
    Optional<Author> findByNameCaseInsensitive(String name);
}
