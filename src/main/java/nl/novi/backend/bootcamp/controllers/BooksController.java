package nl.novi.backend.bootcamp.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.services.BooksService;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import java.util.List;

@Validated
@RestController
@RequestMapping("/books")
public class BooksController {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private BooksService booksService;

    public BooksController(BooksService booksService) {
        this.booksService = booksService;
    }

    @GetMapping
    public List<BookDto> getList() {
        return booksService.getList();
    }

    @GetMapping("/{id}")
    public BookDto getOne(@PathVariable @Max(100) Long id) {
        return booksService.getOne(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable @Max(100) Long id) {
        booksService.delete(id);
    }

    @PostMapping("")
    public BookDto create(@RequestParam("file") MultipartFile file, @RequestParam("data") String data) {
        BookDto bookDto;
        try {
            bookDto = objectMapper.readValue(data, BookDto.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return booksService.create(file, bookDto);
    }

    @PutMapping("/{id}")
    public BookDto update(@PathVariable @Max(100) Long id, @RequestBody BookDto book) {
        return booksService.update(id, book);
    }

    @GetMapping(value = "/{id}/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public Resource downloadCover(@PathVariable @Max(100) Long id) {
        return booksService.downloadCover(id);
    }
}
