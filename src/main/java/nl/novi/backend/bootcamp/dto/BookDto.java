package nl.novi.backend.bootcamp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class BookDto {
    private Long id;
    @NotBlank
    private String title;
    private String subTitle;
    private String filename;
    private List<AuthorDto> authors;
}
