package nl.novi.backend.bootcamp.services;

import nl.novi.backend.bootcamp.exceptions.NotFoundException;
import nl.novi.backend.bootcamp.model.Book;
import nl.novi.backend.bootcamp.repositories.BooksRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BooksServiceTest {

    @Mock
    private BooksRepository booksRepository;

    @InjectMocks
    private BooksService booksService;

    @Test
    public void shouldGetOne() {
        when(booksRepository.findById(1L)).thenReturn(Optional.of(new Book()));
        var book = booksService.getOne(1L);
        assertNotNull(book);
    }

    @Test
    public void shouldThrowException() {
        when(booksRepository.findById(any())).thenReturn(Optional.empty());
        NotFoundException thrown = assertThrows(NotFoundException.class, () -> booksService.getOne(1L));
        assertEquals("Item not found", thrown.getMessage());
        verify(booksRepository, times(1)).findById(1L);
    }

}
