package nl.novi.backend.bootcamp.controllers;


import nl.novi.backend.bootcamp.dto.AuthenticationRequest;
import nl.novi.backend.bootcamp.dto.AuthenticationResponse;
import nl.novi.backend.bootcamp.services.AuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/authenticate")
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest request) {
        String token = authenticationService.authenticateUser(request.getUsername(), request.getPassword());
        return AuthenticationResponse.builder().accessToken(token).build();
    }
}
