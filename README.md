# Spring boot application

## Prerequisites

The following software is needed to run this app
- Docker
- Docker-compose
- Intellij
- Amazon Coretto JDK 17


## Run the application
To start the database run from this directory
````bash
docker-compose up -d
````
then start the application


#### Application credentials
| **Username** | **Password** | **Roles**             |
|--------------|--------------|-----------------------|
| user         | password     | ROLE_USER             |
| admin        | password     | ROLE_USER, ROLE_ADMIN |
| mathijs      | password     | ROLE_USER             |
