INSERT INTO authors (name)
VALUES ('test');
INSERT INTO authors (name)
VALUES ('Bert');
INSERT INTO authors (name)
VALUES ('Camiel');

INSERT INTO book_stores (name)
VALUES ('My Book Store');

INSERT INTO books (sub_title, title, book_store_id)
VALUES ('some test book', 'book title 4', 1);
INSERT INTO books (sub_title, title, book_store_id)
VALUES ('some test book', 'book title 3', 1);

INSERT INTO books_authors (books_id, authors_id)
VALUES (1, 1);
INSERT INTO books_authors (books_id, authors_id)
VALUES (1, 2);
INSERT INTO books_authors (books_id, authors_id)
VALUES (2, 3);
INSERT INTO books_authors (books_id, authors_id)
VALUES (2, 2);

INSERT INTO users (username, password, enabled)
VALUES ('user', '$2a$12$1ceDZ7RM3zHD.86lRxogpO4Jx.PvtDp9RY4xfYzgtT/TWvg03OHxG', TRUE);
INSERT INTO users (username, password, enabled)
VALUES ('admin', '$2a$12$1ceDZ7RM3zHD.86lRxogpO4Jx.PvtDp9RY4xfYzgtT/TWvg03OHxG', TRUE);
INSERT INTO users (username, password, enabled)
VALUES ('mathijs', '$2a$12$1ceDZ7RM3zHD.86lRxogpO4Jx.PvtDp9RY4xfYzgtT/TWvg03OHxG', TRUE);


INSERT INTO authorities (username, authority)
VALUES ('user', 'ROLE_USER');
INSERT INTO authorities (username, authority)
VALUES ('mathijs', 'ROLE_USER');
INSERT INTO authorities (username, authority)
VALUES ('admin', 'ROLE_USER');
INSERT INTO authorities (username, authority)
VALUES ('admin', 'ROLE_ADMIN');
