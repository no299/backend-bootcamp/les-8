package nl.novi.backend.bootcamp.mappers;

import nl.novi.backend.bootcamp.dto.AuthorDto;
import nl.novi.backend.bootcamp.model.Author;

public class AuthorMappers {

    public static AuthorDto toDto(Author author) {
        return AuthorDto.builder()
                .name(author.getName())
                .id(author.getId())
                .build();
    }

    public static Author toEntity(AuthorDto author) {
        return Author.builder()
                .name(author.getName())
                .id(author.getId())
                .build();
    }
}
