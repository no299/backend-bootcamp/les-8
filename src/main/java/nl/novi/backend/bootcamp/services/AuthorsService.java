package nl.novi.backend.bootcamp.services;

import nl.novi.backend.bootcamp.dto.AuthorDto;
import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.exceptions.NotFoundException;
import nl.novi.backend.bootcamp.mappers.AuthorMappers;
import nl.novi.backend.bootcamp.mappers.BookMappers;
import nl.novi.backend.bootcamp.repositories.AuthorsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorsService {

    private AuthorsRepository authorsRepository;

    public AuthorsService(AuthorsRepository authorsRepository) {
        this.authorsRepository = authorsRepository;
    }

    public List<BookDto> getBooks(Long id) {
        return authorsRepository.findById(id)
                .orElseThrow(NotFoundException::new)
                .getBooks()
                .stream()
                .map(BookMappers::toDto)
                .toList();
    }

    public List<BookDto> getBooksForAuthor(Long id) {
        var author = authorsRepository.findById(id).orElseThrow(NotFoundException::new);
        var books = author.getBooks();
        return books.stream().map(BookMappers::toDto).toList();
    }

    public AuthorDto getAuthor(Long id) {
        var author =  authorsRepository.findById(id).orElseThrow(NotFoundException::new);
        return AuthorMappers.toDto(author);
    }
}
