package nl.novi.backend.bootcamp.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class UploadResponse {
    private List<String> filenames;
}
