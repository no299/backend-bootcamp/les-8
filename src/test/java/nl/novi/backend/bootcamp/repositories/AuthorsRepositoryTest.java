package nl.novi.backend.bootcamp.repositories;

import nl.novi.backend.bootcamp.model.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class AuthorsRepositoryTest {

    @Autowired
    private AuthorsRepository authorsRepository;

    @Test
    public void shouldFindAuthorByNameIgnoringCase() {
        Author author = authorsRepository.findByNameCaseInsensitive("bert").get();
        assertEquals(2L, author.getId());
    }

    @Test
    public void shouldFindAuthorByName() {
        Author author = authorsRepository.findByName("Bert").get();
        assertEquals(2L, author.getId());
    }

    @Test
    public void shouldNotFindAuthorByName() {
        Optional<Author> author = authorsRepository.findByName("bert");
        assertTrue(author.isEmpty());
    }

}
