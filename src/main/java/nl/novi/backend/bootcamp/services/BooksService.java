package nl.novi.backend.bootcamp.services;

import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.exceptions.NotFoundException;
import nl.novi.backend.bootcamp.mappers.BookMappers;
import nl.novi.backend.bootcamp.model.Author;
import nl.novi.backend.bootcamp.model.Book;
import nl.novi.backend.bootcamp.model.User;
import nl.novi.backend.bootcamp.repositories.AuthorsRepository;
import nl.novi.backend.bootcamp.repositories.BooksRepository;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;


@Service
public class BooksService {

    private final BooksRepository booksRepository;
    private final AuthorsRepository authorsRepository;

    public BooksService(BooksRepository booksRepository, AuthorsRepository authorsRepository) {
        this.booksRepository = booksRepository;
        this.authorsRepository = authorsRepository;
    }

    public void delete(Long id) {
        booksRepository.deleteById(id);
    }

    public BookDto getOne(Long id) {
        var book =  booksRepository.findById(id).orElseThrow(NotFoundException::new);
        return BookMappers.toDto(book);
    }

    public List<BookDto> getList() {
        return StreamSupport
                .stream(booksRepository.findAll().spliterator(), false)
                .map(BookMappers::toDto)
                .toList();
    }

    public BookDto update(Long id, BookDto book) {
        Book foundBook = booksRepository.findById(id).orElseThrow(NotFoundException::new);
        foundBook.setTitle(book.getTitle());
        foundBook.setSubTitle(book.getSubTitle());
        var saved = booksRepository.save(foundBook);
        return BookMappers.toDto(saved);
    }

    public BookDto create(MultipartFile file, BookDto bookDto) {
        String uuid = UUID.randomUUID().toString();
        String filename = "data/" + uuid + file.getOriginalFilename();
        try {
            file.transferTo(Path.of(filename));
        }catch (IOException e){
            e.printStackTrace();
            throw new UncheckedIOException(e);
        }

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for(var authority: user.getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_ADMIN")) {
                isAdmin = true;
                break;
            }
        }

        Book book = BookMappers.toEntity(bookDto);

        List<Author> authors = new ArrayList<>();
        for (Author author : book.getAuthors()) {
            String authorName = author.getName();
            Optional<Author> saved = authorsRepository.findByName(authorName);
            if (saved.isPresent()) {
                authors.add(saved.get());
            } else {
                Author newAuthor = new Author();
                newAuthor.setName(authorName);
                Author newlyCreatedAuthor = authorsRepository.save(newAuthor);
                authors.add(newlyCreatedAuthor);
            }
        }

        book.setVerified(isAdmin);
        book.setAuthors(authors);
        book.setFilename(filename);
        Book saved = booksRepository.save(book);

        return BookMappers.toDto(saved);
    }

    public Resource downloadCover(Long id){
        Book foundBook = booksRepository.findById(id).orElseThrow(NotFoundException::new);
        return new PathResource(Path.of(foundBook.getFilename()));
    }
}
