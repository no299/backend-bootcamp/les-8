package nl.novi.backend.bootcamp.controllers;

import nl.novi.backend.bootcamp.dto.AuthorDto;
import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.services.AuthorsService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RestController
@RequestMapping("/authors")
public class AuthorsController {

    private AuthorsService authorsService;

    public AuthorsController(AuthorsService authorsService) {
        this.authorsService = authorsService;
    }

    @GetMapping("/{id}/books")
    public List<BookDto> booksForAuthor(@PathVariable Long id) {
        return authorsService.getBooks(id);
    }

    @GetMapping("/{id}")
    public AuthorDto getAuthor(@PathVariable Long id) {
        return authorsService.getAuthor(id);
    }
}
