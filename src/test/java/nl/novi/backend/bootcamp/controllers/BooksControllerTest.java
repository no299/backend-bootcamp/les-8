package nl.novi.backend.bootcamp.controllers;

import nl.novi.backend.bootcamp.exceptions.NotFoundException;
import nl.novi.backend.bootcamp.services.BooksService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BooksControllerTest {

    @Mock
    private BooksService booksService;

    @InjectMocks
    private BooksController booksController;

    @Test
    public void shouldNotFindOne() {
        when(booksService.getOne(1L)).thenThrow(new NotFoundException());
        NotFoundException thrown = assertThrows(NotFoundException.class, () -> booksController.getOne(1L));
        assertEquals("Item not found", thrown.getMessage());
    }

    @Test
    public void shouldDelete(){
        booksController.delete(1L);
        verify(booksService, times(1)).delete(1L);
    }

}
