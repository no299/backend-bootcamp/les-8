package nl.novi.backend.bootcamp.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.novi.backend.bootcamp.dto.AuthenticationResponse;
import nl.novi.backend.bootcamp.dto.BookDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class BooksControllerE2eTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldGetBooks() throws Exception {
        String jsonResponse = mockMvc.perform(post("/authenticate")
                        .contentType("application/json")
                        .content("{\"username\": \"user\", \"password\": \"password\"}")
                )
                .andReturn()
                .getResponse()
                .getContentAsString();
        AuthenticationResponse authResponse = objectMapper.readValue(jsonResponse, AuthenticationResponse.class);

        String response = mockMvc.perform(get("/books")
                        .header("Authorization", "Bearer " + authResponse.getAccessToken())
                )
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<BookDto> books = objectMapper.readValue(response, new TypeReference<List<BookDto>>(){});
        assertEquals(2, books.size());
    }

    @Test
    void shouldNotGetBooks() throws Exception {
        mockMvc.perform(get("/books"))
                .andExpect(result -> assertEquals(403, result.getResponse().getStatus()));
    }
}
