package nl.novi.backend.bootcamp.mappers;

import nl.novi.backend.bootcamp.dto.AuthorDto;
import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.model.Author;
import nl.novi.backend.bootcamp.model.Book;

import java.util.List;

public class BookMappers {

    public static BookDto toDto(Book book) {
        List<AuthorDto> authors = book.getAuthors()
                .stream()
                .map(AuthorMappers::toDto)
                .toList();
        return BookDto.builder()
                .title(book.getTitle())
                .subTitle(book.getSubTitle())
                .filename(book.getFilename())
                .id(book.getId())
                .authors(authors)
                .build();
    }

    public static Book toEntity(BookDto book) {
        List<Author> authors = book.getAuthors()
                .stream()
                .map(AuthorMappers::toEntity)
                .toList();
        return Book.builder()
                .title(book.getTitle())
                .filename(book.getFilename())
                .subTitle(book.getSubTitle())
                .id(book.getId())
                .authors(authors)
                .build();
    }
}
