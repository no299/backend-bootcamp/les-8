package nl.novi.backend.bootcamp.mappers;

import nl.novi.backend.bootcamp.dto.BookDto;
import nl.novi.backend.bootcamp.dto.BookStoreDto;
import nl.novi.backend.bootcamp.model.Book;
import nl.novi.backend.bootcamp.model.BookStore;

import java.util.List;

public class BookStoreMappers {

    public static BookStoreDto toDto(BookStore bookStore) {
        List<BookDto> books = bookStore.getBooks()
                .stream()
                .map(BookMappers::toDto)
                .toList();
        return BookStoreDto.builder()
                .id(bookStore.getId())
                .name(bookStore.getName())
                .books(books)
                .build();
    }

    public static BookStore toEntity(BookStoreDto bookStore) {
        List<Book> books = bookStore.getBooks()
                .stream()
                .map(BookMappers::toEntity)
                .toList();
        return BookStore.builder()
                .id(bookStore.getId())
                .name(bookStore.getName())
                .books(books)
                .build();
    }
}
