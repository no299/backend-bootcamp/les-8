package nl.novi.backend.bootcamp.controllers;

import nl.novi.backend.bootcamp.dto.BookStoreDto;
import nl.novi.backend.bootcamp.services.BookStoreService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;

@Validated
@RestController
@RequestMapping("/bookstores")
public class BookStoreController {

    private BookStoreService bookStoreService;

    public BookStoreController(BookStoreService bookStoreService) {
        this.bookStoreService = bookStoreService;
    }

    @GetMapping("/{id}")
    public BookStoreDto getOne(@PathVariable @Max(100) Long id) {
        return bookStoreService.getOne(id);
    }
}
