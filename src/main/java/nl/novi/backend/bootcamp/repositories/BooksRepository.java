package nl.novi.backend.bootcamp.repositories;

import nl.novi.backend.bootcamp.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends CrudRepository<Book, Long> {
}
