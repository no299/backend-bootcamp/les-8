package nl.novi.backend.bootcamp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String subTitle;

    private Boolean verified;
    private String filename;

    @JsonManagedReference
    @ManyToMany
    private List<Author> authors = new ArrayList<>();

    @JsonManagedReference
    @ManyToOne
    private BookStore bookStore;
}
