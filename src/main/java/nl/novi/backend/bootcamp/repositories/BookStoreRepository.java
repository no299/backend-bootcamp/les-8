package nl.novi.backend.bootcamp.repositories;

import nl.novi.backend.bootcamp.model.BookStore;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookStoreRepository extends CrudRepository<BookStore, Long> {
}
